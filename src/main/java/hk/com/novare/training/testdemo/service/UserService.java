package hk.com.novare.training.testdemo.service;

import hk.com.novare.training.testdemo.model.*;

public interface UserService {
        void sendMail(User user);
}
